package com.example.constraintlayoutprogrammatically

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Guideline
import com.example.constraintlayoutprogrammatically.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val constraintLayout = binding.constraintLayout

        val guideline2 = binding.guideline2
        setGuideline(guideline2, 0.2f)

        val guideline3 = binding.guideline3
        setGuideline(guideline3, 0.8f)

        val guideline4 = binding.guideline4
        setGuideline(guideline4, 0.1f)

        val guideline5 = binding.guideline5
        setGuideline(guideline5, 0.9f)

        // Creo un TextView y lo añado a la vista
        val textView = TextView(this)
        textView.id = View.generateViewId()
        textView.textSize = 20f
        textView.text = "HELLO WORLD!!!"
        constraintLayout.addView(textView)

        //Creo un ConstraintSet a partir del ConstraintLayout.
        //Esto me permite definir un conjunto de restricciones que se aplicarán al texxView dentro del ConstraintLayout
        val set = ConstraintSet()
        set.clone(constraintLayout)
        set.connect(textView.id, ConstraintSet.TOP, guideline2.id, ConstraintSet.TOP)
        set.connect(textView.id, ConstraintSet.START, guideline4.id, ConstraintSet.START)
        set.applyTo(constraintLayout)

        // Creo un ImageView y lo añado a la vista
        val imageView = ImageView(this)
        imageView.id = View.generateViewId()
        imageView.setImageResource(R.drawable.ic_launcher_background)
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        constraintLayout.addView(imageView)

        //Defino las restricciones del imageView dentro del ConstraintLayout
        set.clone(constraintLayout)
        set.connect(imageView.id, ConstraintSet.BOTTOM, guideline3.id, ConstraintSet.TOP)
        set.connect(imageView.id, ConstraintSet.END, guideline5.id, ConstraintSet.START)
        set.constrainHeight(imageView.id, 0)
        set.constrainWidth(imageView.id, 0)
        set.constrainPercentHeight(imageView.id, 0.25f)
        set.constrainPercentWidth(imageView.id, 0.5f)
        set.applyTo(constraintLayout)

    }

    fun setGuideline(guideline: Guideline, percent: Float){
        val params = guideline.layoutParams as ConstraintLayout.LayoutParams
        params.guidePercent = percent //rango de 0 a 1
        guideline.layoutParams = params
    }
}